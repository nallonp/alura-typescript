import { MeuObjeto, Negociacao } from "./index";

export class Negociacoes implements MeuObjeto<Negociacoes> {
  private _negociacoes: Negociacao[] = [];

  adiciona(negociacao: Negociacao) {
    this._negociacoes.push(negociacao);
  }

  paraArray(): Negociacao[] {
    return ([] as Negociacao[]).concat(this._negociacoes);
  }

  paraTexto(): string {
    return JSON.stringify(this);
  }

  ehIgual(outro: Negociacoes): boolean {
    return (
      JSON.stringify(this._negociacoes) == JSON.stringify(outro._negociacoes)
    );
  }
}
