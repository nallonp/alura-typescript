import { MeuObjeto } from "./index";

export class Negociacao implements MeuObjeto<Negociacao> {
  constructor(
    readonly data: Date,
    readonly quantidade: number,
    readonly valor: number
  ) {}

  get volume() {
    return this.quantidade * this.valor;
  }

  paraTexto(): string {
    return `Date: ${this.data}
        Quantidade: ${this.quantidade}
        Valor: ${this.valor}
        Volume: ${this.volume}
        `;
  }

  ehIgual(outro: Negociacao): boolean {
    return this.quantidade === outro.quantidade && this.valor === outro.valor;
  }
}
