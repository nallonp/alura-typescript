import { MensagemView, NegociacoesView } from "../views/index";
import { Negociacao, Negociacoes } from "../models/index";
import { domInject, throttle } from "../helpers/decorators/index";
import { NegociacaoService } from "../services/index";
import { imprime } from "../helpers/index";

export class NegociacaoController {
  @domInject("#data")
  private _inputData: JQuery;
  @domInject("#quantidade")
  private _inputQuantidade: JQuery;
  @domInject("#valor")
  private _inputValor: JQuery;
  private _negociacoes = new Negociacoes();
  private _negociacoesView = new NegociacoesView("#negociacoesView", true);
  private _mensagemView = new MensagemView("#mensagemView", true);
  private _negociacaoService = new NegociacaoService();

  constructor() {
    this._negociacoesView.update(this._negociacoes);
  }

  @throttle()
  adiciona() {
    let data = new Date(this._inputData.val().replace(/-/, ","));
    if (this._ehDiaUtil(data)) {
      this._mensagemView.update("Somente negociações em dias úteis por favor!");
      return;
    }
    const negociacao = new Negociacao(
      data,
      parseInt(this._inputQuantidade.val()),
      parseFloat(this._inputValor.val())
    );
    this._negociacoes.adiciona(negociacao);
    this._negociacoesView.update(this._negociacoes);
    this._mensagemView.update("Negociação adicionada com sucesso.");
    imprime(negociacao, this._negociacoes);
  }

  @throttle()
  async importaDados() {
    try {
      const negociacoesParaImportar =
        await this._negociacaoService.obterNegociacoes((res) => {
          if (res.ok) {
            return res;
          } else {
            throw new Error(res.statusText);
          }
        });

      const negociacoesJaImportadas = this._negociacoes.paraArray();
      negociacoesParaImportar
        .filter(
          (negociacao) =>
            !negociacoesJaImportadas.some((jaImportada) =>
              negociacao.ehIgual(jaImportada)
            )
        )
        .forEach((negociacao) => this._negociacoes.adiciona(negociacao));
      this._negociacoesView.update(this._negociacoes);
    } catch (err) {
      this._mensagemView.update(err.message);
    }
  }

  private _ehDiaUtil(data: Date): boolean {
    return (
      data.getDay() === DiaDaSemana.Domingo ||
      data.getDay() === DiaDaSemana.Sabado
    );
  }
}

enum DiaDaSemana {
  Domingo,
  Segunda,
  Terca,
  Quarta,
  Quinta,
  Sexta,
  Sabado,
}
