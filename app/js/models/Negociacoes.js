System.register([], function (exports_1, context_1) {
  "use strict";
  var __moduleName = context_1 && context_1.id;
  var Negociacoes;
  return {
    setters: [],
    execute: function () {
      Negociacoes = class Negociacoes {
        constructor() {
          this._negociacoes = [];
        }

        adiciona(negociacao) {
          this._negociacoes.push(negociacao);
        }

        paraArray() {
          return [].concat(this._negociacoes);
        }

        paraTexto() {
          return JSON.stringify(this);
        }

        ehIgual(outro) {
          return (
            JSON.stringify(this._negociacoes) ==
            JSON.stringify(outro._negociacoes)
          );
        }
      };
      exports_1("Negociacoes", Negociacoes);
    },
  };
});
