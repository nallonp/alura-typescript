System.register([], function (exports_1, context_1) {
  "use strict";
  var __moduleName = context_1 && context_1.id;
  var Negociacao;
  return {
    setters: [],
    execute: function () {
      Negociacao = class Negociacao {
        constructor(data, quantidade, valor) {
          this.data = data;
          this.quantidade = quantidade;
          this.valor = valor;
        }

        get volume() {
          return this.quantidade * this.valor;
        }

        paraTexto() {
          return `Date: ${this.data}
        Quantidade: ${this.quantidade}
        Valor: ${this.valor}
        Volume: ${this.volume}
        `;
        }

        ehIgual(outro) {
          return (
            this.quantidade === outro.quantidade && this.valor === outro.valor
          );
        }
      };
      exports_1("Negociacao", Negociacao);
    },
  };
});
